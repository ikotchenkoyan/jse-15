package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER STATUS]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusName = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusName);
        getProjectService().changeProjectStatusByIndex(index, status);
    }

    @Override
    public String getDescription() {
        return "Change project status by index.";
    }

    @Override
    public String getName() {
        return "project-change-status-by-index";
    }

}
