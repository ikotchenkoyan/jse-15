package ru.t1.kotchenko.tm.command.user;

import ru.t1.kotchenko.tm.api.service.IAuthService;
import ru.t1.kotchenko.tm.api.service.IUserService;
import ru.t1.kotchenko.tm.command.AbstractCommand;
import ru.t1.kotchenko.tm.exception.entity.UserNotFoundException;
import ru.t1.kotchenko.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    public IAuthService getAuthService() {
        return getServiceLocator().getAuthTaskService();
    }

    public String getArgument() {
        return null;
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

}
