package ru.t1.kotchenko.tm.command.user;

import ru.t1.kotchenko.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "update profile of current user";
    }

    @Override
    public String getName() {
        return "update-user-profile";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("[ENTER FIRST NAME:]");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        final String lasttName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME:]");
        final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lasttName, middleName);
    }
}
