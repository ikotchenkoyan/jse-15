package ru.t1.kotchenko.tm.command.system;

import ru.t1.kotchenko.tm.api.service.ICommandService;
import ru.t1.kotchenko.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
