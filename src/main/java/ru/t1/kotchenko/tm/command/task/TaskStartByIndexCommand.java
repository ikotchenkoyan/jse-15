package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @Override
    public String getName() {
        return "task-start-by-index";
    }

}
