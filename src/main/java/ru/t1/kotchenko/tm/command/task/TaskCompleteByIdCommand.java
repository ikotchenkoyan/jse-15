package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

}
