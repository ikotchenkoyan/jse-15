package ru.t1.kotchenko.tm.command.project;

import ru.t1.kotchenko.tm.model.Project;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        showProject(project);
    }

    @Override
    public String getDescription() {
        return "Show project by index.";
    }

    @Override
    public String getName() {
        return "project-show-by-id";
    }

}
