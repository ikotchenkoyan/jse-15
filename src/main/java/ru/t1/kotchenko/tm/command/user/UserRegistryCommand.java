package ru.t1.kotchenko.tm.command.user;

import ru.t1.kotchenko.tm.api.service.IAuthService;
import ru.t1.kotchenko.tm.model.User;
import ru.t1.kotchenko.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String getDescription() {
        return "registry user";
    }

    @Override
    public String getName() {
        return "user-registry";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }
}
