package ru.t1.kotchenko.tm.command.system;

public class ApplicationDeveloperCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Ivan Kotchenko");
        System.out.println("E-MAIL: IvanKotchenko@yandex.ru");
    }

    @Override
    public String getArgument() {
        return "-d";
    }

    @Override
    public String getDescription() {
        return "Show developer info.";
    }

    @Override
    public String getName() {
        return "developer";
    }

}
