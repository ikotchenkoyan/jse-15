package ru.t1.kotchenko.tm.command.task;

import ru.t1.kotchenko.tm.api.service.IProjectTaskService;
import ru.t1.kotchenko.tm.api.service.ITaskService;
import ru.t1.kotchenko.tm.command.AbstractCommand;
import ru.t1.kotchenko.tm.enumerated.Status;
import ru.t1.kotchenko.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    public String getArgument() {
        return null;
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            final String name = task.getName();
            final String description = task.getDescription();
            System.out.printf("%s. %s : %s \n", index, name, description);
            index++;
        }
    }

}
