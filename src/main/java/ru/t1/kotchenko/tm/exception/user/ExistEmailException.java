package ru.t1.kotchenko.tm.exception.user;

public final class ExistEmailException extends AbstractUserException {

    public ExistEmailException() {
        super("Error! Email is exist.");
    }

}
