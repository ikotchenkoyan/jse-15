package ru.t1.kotchenko.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Error! Status is empty.");
    }

}
