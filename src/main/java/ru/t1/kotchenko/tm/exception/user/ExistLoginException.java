package ru.t1.kotchenko.tm.exception.user;

public final class ExistLoginException extends AbstractUserException {

    public ExistLoginException() {
        super("Error! Login is exist.");
    }

}
