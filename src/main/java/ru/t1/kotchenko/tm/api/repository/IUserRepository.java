package ru.t1.kotchenko.tm.api.repository;

import ru.t1.kotchenko.tm.model.User;

import java.util.List;

public interface IUserRepository {

    User add(User user);

    List<User> findAll();

    User findOneById(String id);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User remove(User user);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
