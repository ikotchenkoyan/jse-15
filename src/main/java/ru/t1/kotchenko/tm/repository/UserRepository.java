package ru.t1.kotchenko.tm.repository;

import ru.t1.kotchenko.tm.api.repository.IUserRepository;
import ru.t1.kotchenko.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public User add(User user) {
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findOneById(String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findOneByLogin(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findOneByEmail(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User remove(User user) {
        users.remove(user);
        return user;
    }

    @Override
    public Boolean isLoginExist(String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(String email) {
        for (final User user : users) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }
}
